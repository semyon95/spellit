module Server

open Froto.Serialization
open FSharpPlus.Data
open Suave
open Suave.Filters
open Suave.Operators
open Suave.RequestErrors
open Suave.Sockets
open Suave.Sockets.Control
open Suave.WebSocket
open System
open System.Security.Cryptography

open Coordinator
open Coordinator.Agent
open Coordinator.Dto
open Coordinator.State
open Utils

type AppContext = {
    Coordinator : CoordinatorAgent
    }

let sha256 (data : byte[]) : byte[] =
    use sha256 = SHA256.Create()
    data |> sha256.ComputeHash

let tokenIntoParticipantId (token : Guid) : Coordinator.Domain.ParticipantId =
    token.ToByteArray()
    |> sha256
    |> Convert.ToBase64String
    |> Coordinator.Domain.ParticipantId

let sendProtoMessage
    (webSocket : WebSocket)
    (buffer : ZeroCopyBuffer)
    (msg : Proto.ServerMessage)
    : SocketOp<unit> =

    Proto.ServerMessage.Serialize (msg, buffer)
    webSocket.send Binary (buffer.AsArraySegment) true

let ws
    (coordinator : CoordinatorAgent)
    (token : Guid)
    (webSocket : WebSocket)
    (_ : HttpContext) : SocketOp<unit> =

    let participantId = token |> tokenIntoParticipantId

    let withCoordinatorTx (tx : CoordinatorTx) : SocketOp<unit> =
        let emptyResponse = [||] |> ByteSegment

        let heartbeat =
            socket {
                while true do
                    do! Async.Sleep Settings.heartbeatIntervalMs |> SocketOp.ofAsync
                    do! webSocket.send Ping emptyResponse true
            } |> Async.map Some

        let listener =
            let buffer = new ZeroCopyBuffer(Array.zeroCreate Settings.msgBufferSize)

            socket {
                let mutable loop = true
                while loop do
                    let! incomingMsg =
                        webSocket.read ()
                        |> Async.withTimeout (Settings.heartbeatIntervalMs * 2)
                        |> SocketOp.ofAsync
                    
                    match incomingMsg with
                    | None ->
                        do! webSocket.send Close emptyResponse true
                        loop <- false
                    | Some socketOp ->
                        match! Async.result socketOp with
                        | (Binary, data, true) ->
                            let protoMsg = new ZeroCopyBuffer(data) |> Proto.ClientMessagePayload.Deserialize
                            
                            printfn "incoming from '%s':\n%A" (token.ToString()) protoMsg

                            match domainFromProto protoMsg with
                            | DomainEvent payload ->
                                tx payload
                            | ErrorResponseMessage protoErrorMsg ->
                                printfn "- error response:\n%A" protoErrorMsg

                                do! sendProtoMessage webSocket buffer protoErrorMsg

                        | (Ping, data, _) ->
                            do! webSocket.send Pong (data |> ByteSegment) true

                        | (Close, _, _) ->
                            do! webSocket.send Close emptyResponse true
                            loop <- false

                        | _ -> ()
            } |> Async.map Some

        Async.Choice [| heartbeat; listener |]
        |> Async.map (Option.defaultValue (Choice1Of2 ()))

    let handleOutcomingMessage (payload : OutcomingMessagePayload) : Async<_> =
        let buffer = new ZeroCopyBuffer(Array.zeroCreate Settings.msgBufferSize)
        let protoMsg = protoFromDomain payload

        printfn "outcoming to '%s':\n%A" (token.ToString()) protoMsg

        sendProtoMessage webSocket buffer protoMsg

    Coordinator.Agent.withChannel coordinator withCoordinatorTx handleOutcomingMessage participantId

let handleWs (coordinator : CoordinatorAgent) : WebPart = request (fun r ->
    let token =
        r.queryParamOpt "token"
        |> Option.bind (fun (_, token) -> token)
        |> Option.bind (asOptionFn Guid.TryParse)

    match token with
    | None -> BAD_REQUEST ""
    | Some token -> handShake (ws coordinator token)
    )

let app (context : AppContext) : WebPart = 
    choose [
        path "/v1/ws" >=> handleWs context.Coordinator
        NOT_FOUND ""
        ]
