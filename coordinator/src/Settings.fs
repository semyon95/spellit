module Settings

open NodaTime
open System

let maxSocketOps : int = 1000
let maxContentLength : int = 10240

let msgBufferSize : int = 8192
let heartbeatIntervalMs : int = 2000

let cleanupInterval : TimeSpan = TimeSpan.FromSeconds 1.0

let sessionTimeout : Duration = Duration.FromSeconds 5.0
let baseDelayMs : double = 1000.0
let offsetDelayMs : double = 50.0

