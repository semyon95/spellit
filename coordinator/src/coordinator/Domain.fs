module Coordinator.Domain

open FSharpPlus
open FSharpPlus.Data
open NodaTime
open System

type ChannelName = private ChannelName of string
let (|ChannelName|) (ChannelName n) = n

module ChannelName =
    type ValidationError = DisallowedCharacters of unit

    let create (name : string) : Result<ChannelName, ValidationError> =
        let isValid = name.ToCharArray() |> Array.forall Char.IsLetterOrDigit
        if isValid
        then
            let name = name.ToLowerInvariant()
            Ok (ChannelName name)
        else
            Error (DisallowedCharacters ())

type RoomWord = private RoomWord of string
let (|RoomWord|) (RoomWord w) = w

module RoomWord =
    type InvalidLength = {
        minLength : int
        maxLength : int
        }

    type ValidationError =
        | DisallowedCharacters of unit
        | InvalidLength of InvalidLength

    let minLength = 3
    let maxLength = 12

    let inner (roomWord : RoomWord) : string =
        match roomWord with RoomWord word -> word

    let create (name : string) : ValidationAcc<ValidationError, RoomWord> = 
        let charsValidator =
            if name.ToCharArray() |> Array.forall Char.IsLetterOrDigit
            then ValidationAcc.success ()
            else ValidationAcc.failure (DisallowedCharacters ())

        let lengthValidator =
            if name.Length >= minLength && name.Length <= maxLength
            then ValidationAcc.success ()
            else ValidationAcc.failure (InvalidLength { minLength = minLength; maxLength = maxLength })
        
        konst (RoomWord (name.ToUpperInvariant()))
        <!> charsValidator
        *> lengthValidator

    let length : RoomWord -> int =
        function
        | RoomWord word -> String.length word

type ParticipantId = ParticipantId of string

[<NoEquality; NoComparison>]
type Room = private {
    Word : RoomWord
    Participants : Set<ParticipantId>
    }

module Room =
    type AddParticipantResult =
        | ParticipantAdded of Room
        | LastParticipantAdded of RoomWord * ParticipantId[]
        | RoomIsFull

    let word (room : Room) : RoomWord =
        room.Word

    let participants (room : Room) : Set<ParticipantId> =
        room.Participants

    let maxParticipants (room : Room) : int =
        RoomWord.length room.Word

    let create (word : RoomWord) (participant : ParticipantId) : Room =
        {
            Word = word
            Participants = Set.singleton participant
        }

    let addParticipant (participantId : ParticipantId) (room : Room) : AddParticipantResult =
        let participants = room.Participants |> Set.add participantId
        
        let participantCount = Set.count participants
        let maxParticipants = maxParticipants room

        if participantCount > maxParticipants
        then RoomIsFull
        else if participantCount = maxParticipants
        then LastParticipantAdded (room.Word, participants |> Set.toArray)
        else ParticipantAdded { room with Participants = participants }

    let removeParticipant (participantId : ParticipantId) (room : Room) : Option<Room> =
        let participants = room.Participants |> Set.remove participantId

        if Set.isEmpty participants
        then None
        else Some { room with Participants = participants }

type Location = {
    Channel : ChannelName
    Word : RoomWord
}

module Location =
    type ValidationError =
        | Channel of ChannelName.ValidationError
        | Word of NonEmptyList<RoomWord.ValidationError>

    let create (channel : string) (word : string) : ValidationAcc<ValidationError, Location> =
        let channelValidation =
            ChannelName.create channel
            |> ValidationAcc.fromResult Channel
        
        let wordValidation =
            RoomWord.create word
            |> ValidationAcc.mapErrors Word

        (fun channel word -> { Channel = channel; Word = word })
        <!> channelValidation
        <*> wordValidation

type RoomInfo =
    { Location : Location
      ParticipantCount : int
      MaxParticipants : int
    }

module RoomInfo =
    let create (channelName : ChannelName) (room : Room) : RoomInfo =
        { Location = { Channel = channelName; Word = room |> Room.word };
          ParticipantCount = Room.participants room |> Set.count;
          MaxParticipants = Room.maxParticipants room;
        }

type RoomList =
    { Channel : ChannelName
      Rooms : RoomInfo[]
    }

[<NoEquality; NoComparison>]
type Channel = private {
    Name : ChannelName
    Rooms : NonEmptyList<Room>
    }

module Channel =
    let create (name : ChannelName) (room : Room) : Channel =
        { Name = name; Rooms = NonEmptyList.singleton room }

[<NoEquality; NoComparison>]
type Channels = private Channels of Map<ChannelName, Channel>

module Channels =
    type AddParticipantResult =
        | ParticipantAdded of Room * Channels
        | LastParticipantAdded of RoomWord * ParticipantId[] * Channels
        | RoomIsFull

    let empty : Channels =
        Channels (Map.empty)

    let getRoomList (channelName : ChannelName) : Channels -> RoomList =
        function
        | Channels channels ->
            let rooms =
                match channels |> Map.tryFind channelName with
                | None -> Seq.empty
                | Some channel ->
                    channel.Rooms
                    |> NonEmptyList.toSeq
                    |> Seq.map (RoomInfo.create channelName)
                    |> Seq.sortWith (fun a b ->
                        match compare b.ParticipantCount a.ParticipantCount with
                        | i when i <> 0 -> i
                        | _ -> compare a.MaxParticipants b.MaxParticipants
                        )

            { Channel = channelName;
              Rooms = rooms |> ofSeq;
            }

    let addParticipant
        (participantId : ParticipantId)
        (location : Location)
        : Channels -> AddParticipantResult =

        function
        | Channels channels ->
            match channels |> Map.tryFind location.Channel with
            | None ->
                let targetRoom = Room.create location.Word participantId
                let channel = Channel.create location.Channel targetRoom
                let channels = Channels (channels |> Map.add location.Channel channel)
                ParticipantAdded (targetRoom, channels)

            | Some channel ->
                let targetRoom, otherRooms =
                    channel.Rooms
                    |> NonEmptyList.toList
                    |> List.partition (fun room -> room.Word = location.Word)

                match targetRoom with
                | [] ->
                    let targetRoom = Room.create location.Word participantId
                    let channel = { channel with Rooms = NonEmptyList.create targetRoom otherRooms }
                    let channels = Channels (channels |> Map.add location.Channel channel)
                    ParticipantAdded (targetRoom, channels)

                | room :: _ ->
                    match room |> Room.addParticipant participantId with
                    | Room.ParticipantAdded targetRoom ->
                        let channel = { channel with Rooms = NonEmptyList.create targetRoom otherRooms }
                        let channels = Channels (channels |> Map.add location.Channel channel)
                        ParticipantAdded (targetRoom, channels)

                    | Room.LastParticipantAdded (word, participantIds) ->
                        let channels =
                            match otherRooms with
                            | [] ->
                                Channels (channels |> Map.remove location.Channel)
                            | otherRoom :: restOtherRooms ->
                                let channel = { channel with Rooms = NonEmptyList.create otherRoom restOtherRooms }
                                Channels (channels |> Map.add location.Channel channel)
                        LastParticipantAdded (word, participantIds, channels)

                    | Room.RoomIsFull -> RoomIsFull

    let removeParticipant
        (participantId : ParticipantId)
        (location : Location)
        : Channels -> Option<Room> * Channels =

        function
        | Channels channels ->
            match channels |> Map.tryFind location.Channel with
            | None -> (None, Channels channels)
            | Some channel ->
                let targetRoom, otherRooms =
                    channel.Rooms
                    |> NonEmptyList.toList
                    |> List.partition (fun room -> room.Word = location.Word)

                let targetRoom, rooms =
                    match targetRoom with
                    | [] -> (None, otherRooms)
                    | room :: _ ->
                        let room = room |> Room.removeParticipant participantId
                        match room with
                        | None -> (None, otherRooms)
                        | Some room -> (Some room, room :: otherRooms)

                match rooms with
                | [] -> (None, Channels (channels |> Map.remove location.Channel))
                | first :: rest ->
                    let rooms = NonEmptyList.create first rest
                    let channel = { channel with Rooms = rooms }
                    let channels = Channels (channels |> Map.add location.Channel channel)
                    (targetRoom, channels)

    let removeRoom (location : Location) : Channels -> Channels =
        function
        | Channels channels ->
            match channels |> Map.tryFind location.Channel with
            | None -> Channels channels
            | Some channel ->
                let remainingRooms =
                    channel.Rooms
                    |> NonEmptyList.toList
                    |> List.filter (fun room -> room.Word <> location.Word)

                match remainingRooms with
                | [] -> Channels (channels |> Map.remove location.Channel)
                | first :: rest ->
                    let rooms = NonEmptyList.create first rest
                    let channel = { channel with Rooms = rooms }
                    Channels (channels |> Map.add location.Channel channel)

[<NoEquality; NoComparison>]
type Participants = private Participants of Map<ParticipantId, Location>

module Participants =
    let empty : Participants =
        Participants (Map.empty)

    let add (participantId : ParticipantId) (location : Location) : Participants -> Participants =
        function
        | Participants participants ->
            Participants (participants |> Map.add participantId location)

    let remove (participantId : ParticipantId) : Participants -> Option<Location> * Participants =
        function
        | Participants participants ->
            let location = participants |> Map.tryFind participantId
            let participants = participants |> Map.remove participantId
            (location, Participants participants)

type CoordinationSessionId = CoordinationSessionId of Guid

type CoordinationSession = private {
    Id : CoordinationSessionId
    StartTimestamp : Instant
    Word : RoomWord
    AllowedParticipantIds : ParticipantId[]
    ApproxLatenciesMs : Map<ParticipantId, double>
    }

type ParticipantInstructions =
    { SessionId : CoordinationSessionId
      PayloadLetter : string
      ApproxServerToClientLatencyMs : double
      BaseDelayMs : double
      OffsetDelayMs : double
      CalculatedFinalWaitTimeMs : double
    }

type AbortedSession =
    { SessionId : CoordinationSessionId
      LaggyParticipantId : ParticipantId
      AllParticipantIds : ParticipantId[]
    }

module CoordinationSession =
    type MeasureLatencyOutcome =
        | LatencyMeasured of CoordinationSession
        | LastLatencyMeasured of List<ParticipantId * ParticipantInstructions>

    type MeasureLatencyError =
        | UnauthorizedParticipant of ParticipantId
        | LaggyParticipant of AbortedSession

    let id (session : CoordinationSession) : CoordinationSessionId =
        session.Id

    let startTimestamp (session : CoordinationSession) : Instant =
        session.StartTimestamp

    let participantIds (session : CoordinationSession) : ParticipantId[] =
        session.AllowedParticipantIds

    let create (word : RoomWord) (participantIds : ParticipantId[]) : Result<CoordinationSession, unit> =
        let sameLength =
            match word with
            | RoomWord word -> String.length word = Array.length participantIds

        if sameLength then
            { Id = Guid.NewGuid() |> CoordinationSessionId;
              StartTimestamp = SystemClock.Instance.GetCurrentInstant();
              Word = word;
              AllowedParticipantIds = participantIds;
              ApproxLatenciesMs = Map.empty;
            } |> Ok
        else
            Error ()

    let measureLatency
        (participantId : ParticipantId)
        (session : CoordinationSession)
        : Result<MeasureLatencyOutcome, MeasureLatencyError> =

        monad' {
            if session.AllowedParticipantIds |> (not << Array.contains participantId)
            then
                return! Error (UnauthorizedParticipant participantId)

            let responseTimestamp = SystemClock.Instance.GetCurrentInstant()
            let approxLatencyMs =
                Instant.Subtract (responseTimestamp, session.StartTimestamp)
                |> (fun duration -> duration.TotalMilliseconds / 2.0)
            
            if approxLatencyMs >= Settings.baseDelayMs
            then
                let abortedSession =
                    { SessionId = session.Id;
                      LaggyParticipantId = participantId;
                      AllParticipantIds = session.AllowedParticipantIds;
                    }
                return! Error (LaggyParticipant abortedSession)

            let approxLatenciesMs = session.ApproxLatenciesMs |> Map.add participantId approxLatencyMs

            if Map.count approxLatenciesMs < Array.length session.AllowedParticipantIds
            then
                let session = { session with ApproxLatenciesMs = approxLatenciesMs }
                return! Ok (LatencyMeasured session)
            else
                return! approxLatenciesMs
                |> Map.toSeq
                |> Seq.zip (RoomWord.inner session.Word)
                |> Seq.mapi (fun index (letter, (participantId, participantApproxLatencyMs)) ->
                    let participantOffsetDelayMs = Settings.offsetDelayMs * double index
                    let finalWaitTime = Settings.baseDelayMs + participantOffsetDelayMs - participantApproxLatencyMs

                    let instructions =
                        { SessionId = session.Id;
                          PayloadLetter = string letter;
                          ApproxServerToClientLatencyMs = participantApproxLatencyMs;
                          BaseDelayMs = Settings.baseDelayMs;
                          OffsetDelayMs = participantOffsetDelayMs;
                          CalculatedFinalWaitTimeMs = finalWaitTime;
                        }
                        
                    (participantId, instructions))
                |> List.ofSeq
                |> (Ok << LastLatencyMeasured)
        }

[<NoEquality; NoComparison>]
type CoordinationSessions = private CoordinationSessions of Map<CoordinationSessionId, CoordinationSession>
let (|CoordinationSessions|) (CoordinationSessions sessions) = sessions

module CoordinationSessions =
    type MeasureLatencyOutcome =
        | LatencyMeasured of CoordinationSessions
        | LastLatencyMeasured of List<ParticipantId * ParticipantInstructions> * CoordinationSessions

    type MeasureLatencyError =
        | NoSuchSession of CoordinationSessionId
        | UnauthorizedParticipant of ParticipantId * CoordinationSessions
        | LaggyParticipant of AbortedSession * CoordinationSessions

    let empty : CoordinationSessions = CoordinationSessions Map.empty

    let count (sessions : CoordinationSessions) : int =
        match sessions with CoordinationSessions sessions -> Map.count sessions

    let addSession (session : CoordinationSession)
        : CoordinationSessions -> CoordinationSessions =
        
        function
        | CoordinationSessions sessions ->
            sessions
            |> Map.add session.Id session
            |> CoordinationSessions

    let removeSessions (sessionIds : CoordinationSessionId[])
        : CoordinationSessions -> List<CoordinationSession> * CoordinationSessions =

        function
        | CoordinationSessions sessions ->
            let droppedSessions, activeSessions =
                sessionIds
                |> Array.fold (fun (droppedSessions, activeSessions) sessionId ->
                    let droppedSession = sessions |> Map.tryFind sessionId
                    match droppedSession with
                    | None -> (droppedSessions, activeSessions)
                    | Some droppedSession ->
                        let activeSessions = activeSessions |> Map.remove sessionId
                        let droppedSessions = droppedSession :: droppedSessions
                        (droppedSessions, activeSessions)
                    )
                    ([], sessions)
            
            (droppedSessions, CoordinationSessions activeSessions)

    let measureLatency (participantId : ParticipantId) (sessionId : CoordinationSessionId)
        : CoordinationSessions -> Result<MeasureLatencyOutcome, MeasureLatencyError> =

        function
        | CoordinationSessions sessions ->
            monad' {
                let! session =
                    sessions
                    |> Map.tryFind sessionId
                    |> Option.toResultWith (NoSuchSession sessionId)

                return! session
                |> CoordinationSession.measureLatency participantId
                |> Result.map (function
                    | CoordinationSession.LatencyMeasured session ->
                        let sessions = sessions |> Map.add sessionId session |> CoordinationSessions
                        LatencyMeasured sessions
                    | CoordinationSession.LastLatencyMeasured instructions ->
                        let sessions = sessions |> Map.remove sessionId |> CoordinationSessions
                        LastLatencyMeasured (instructions, sessions))
                |> Result.mapError (function
                    | CoordinationSession.UnauthorizedParticipant participantId ->
                        UnauthorizedParticipant (participantId, sessions |> CoordinationSessions)
                    | CoordinationSession.LaggyParticipant abortedSession ->
                        let sessions = sessions |> Map.remove sessionId |> CoordinationSessions
                        LaggyParticipant (abortedSession, sessions))
            }

type SessionDeadlines = private SessionDeadlines of List<Instant * CoordinationSessionId>
let (|SessionDeadlines|) (SessionDeadlines deadlines) = deadlines

module SessionDeadlines =
    let empty : SessionDeadlines = SessionDeadlines []

    let add (startTimestamp : Instant) (sessionId : CoordinationSessionId)
        : SessionDeadlines -> SessionDeadlines =
        
        function
        | SessionDeadlines deadlines ->
            let sessionDeadline = (startTimestamp + Settings.sessionTimeout, sessionId)
            SessionDeadlines (sessionDeadline :: deadlines)

    let dropExpired (instant : Instant) : SessionDeadlines -> CoordinationSessionId[] * SessionDeadlines =

        function
        | SessionDeadlines deadlines ->
            let liveDeadlines, pastDeadlines =
                deadlines
                |> List.partition (fun (deadline, _) -> deadline > instant)

            let droppedSessionIds = pastDeadlines |> List.ofSeq |> map snd |> toArray

            (droppedSessionIds, SessionDeadlines liveDeadlines)
