module Coordinator.State

open Coordinator.Domain
open NodaTime

type IncomingMessagePayload =
    | GetRoomList of ChannelName
    | RoomJoinRequest of Location
    | RoomLeaveRequest of unit
    | SessionPong of CoordinationSessionId

type IncomingMessage = {
    ParticipantId : ParticipantId
    Payload : IncomingMessagePayload
    }

type SessionPing = {
    SessionId : CoordinationSessionId
    }

type OutcomingMessagePayload =
    | RoomList of RoomList
    | LeftRoom of unit
    | JoinedRoom of RoomInfo
    | RoomIsFull of Location
    | RoomUpdated of RoomInfo
    | SessionPing of CoordinationSessionId
    | SessionDropped of CoordinationSessionId
    | Instructions of ParticipantInstructions

type OutcomingMessage = {
    ParticipantId : ParticipantId
    Payload : OutcomingMessagePayload
    }

type State = private {
    Channels : Channels
    Participants : Participants
    CoordinationSessions : CoordinationSessions
    SessionDeadlines : SessionDeadlines
    }

let empty : State =
    { Channels = Channels.empty;
      Participants = Participants.empty;
      CoordinationSessions = CoordinationSessions.empty;
      SessionDeadlines = SessionDeadlines.empty;
    }

type StateUpdate = State -> List<OutcomingMessage> * State

let getRoomList (participantId : ParticipantId) (channelName : ChannelName) : StateUpdate = fun state ->
    let roomList = state.Channels |> Channels.getRoomList channelName
    let msgs = [{ ParticipantId = participantId; Payload = RoomList roomList }]
    (msgs, state)

let removeParticipant (participantId : ParticipantId) : StateUpdate = fun state ->
    let { Channels = channels; Participants = participants } = state

    let location, participants = participants |> Participants.remove participantId
    let roomLocation, channels =
        match location with
        | None -> (None, channels)
        | Some location ->
            let room, channels = channels |> Channels.removeParticipant participantId location
            let roomLocation = room |> Option.map (fun room -> (room, location))
            (roomLocation, channels)

    let leftRoomMsg = { ParticipantId = participantId; Payload = LeftRoom () }
    let roomUpdatedMsgs =
        match roomLocation with
        | None -> []
        | Some (room, location) ->
            room
            |> Room.participants
            |> Set.toList
            |> List.map (fun participantId ->
                { ParticipantId = participantId;
                  Payload = RoomUpdated (RoomInfo.create location.Channel room)
                })
    
    let msgs = leftRoomMsg :: roomUpdatedMsgs
    let state = { state with Channels = channels; Participants = participants }
    
    (msgs, state)

let addParticipant (participantId : ParticipantId) (location : Location) (state : State)
    : List<OutcomingMessage> * State =

    let { Channels = channels; Participants = participants } = state
    
    let result = channels |> Channels.addParticipant participantId location
    
    match result with
    | Channels.RoomIsFull ->
        let msgs = [{
            ParticipantId = participantId;
            Payload = RoomIsFull location;
            }]
        let state = { state with Channels = channels; Participants = participants }

        (msgs, state)

    | Channels.ParticipantAdded (room, channels) ->
        let participants = participants |> Participants.add participantId location

        let joinedRoomMsg =
            { ParticipantId = participantId
              Payload = JoinedRoom (RoomInfo.create location.Channel room)
            }

        let roomUpdatedMsgs =
            room
            |> Room.participants
            |> Set.toList
            |> List.filter ((<>) participantId)
            |> List.map (fun participantId ->
                { ParticipantId = participantId;
                  Payload = RoomUpdated (RoomInfo.create location.Channel room)
                })

        let msgs = joinedRoomMsg :: roomUpdatedMsgs
        let state = { state with Channels = channels; Participants = participants }

        (msgs, state)

    | Channels.LastParticipantAdded (word, participantIds, channels) ->
        match CoordinationSession.create word participantIds with
        | Ok session ->
            let sessions = state.CoordinationSessions |> CoordinationSessions.addSession session

            let sessionId = CoordinationSession.id session
            let startTimestamp = CoordinationSession.startTimestamp session

            let sessionDeadlines = state.SessionDeadlines |> SessionDeadlines.add startTimestamp sessionId

            let state =
                { state with
                    Channels = channels;
                    CoordinationSessions = sessions;
                    SessionDeadlines = sessionDeadlines;
                }

            let participantCount = participantIds |> Array.length
            let roomInfo =
                { Location = location;
                  ParticipantCount = participantCount;
                  MaxParticipants = participantCount;
                }

            let joinedRoomMsg =
                { ParticipantId = participantId
                  Payload = JoinedRoom roomInfo
                }

            let roomUpdatedMsgs =
                participantIds
                |> Array.ofSeq
                |> Seq.filter ((<>) participantId)
                |> Seq.map (fun participantId ->
                    {
                        ParticipantId = participantId;
                        Payload = RoomUpdated roomInfo
                    })
                |> List.ofSeq
            
            let sessionPingMsgs =
                participantIds
                |> List.ofArray
                |> List.map (fun id ->
                    { ParticipantId = id;
                      Payload = SessionPing (CoordinationSession.id session);
                    })

            let msgs = joinedRoomMsg :: List.append roomUpdatedMsgs sessionPingMsgs
            
            (msgs, state)
        | Error () ->
            ([], state)

let registerSessionParticipant (participantId : ParticipantId) (sessionId : CoordinationSessionId) : StateUpdate = fun state ->
    let sessions = state.CoordinationSessions

    let registrationResult = sessions |> CoordinationSessions.measureLatency participantId sessionId
    match registrationResult with
    | Error (CoordinationSessions.NoSuchSession _) -> ([], state)
    | Error (CoordinationSessions.UnauthorizedParticipant _) -> ([], state)
    | Error (CoordinationSessions.LaggyParticipant (abortedSession, sessions)) ->
        let msgs =
            abortedSession.AllParticipantIds
            |> Array.toSeq
            |> Seq.map (fun participantId ->
                { ParticipantId = participantId;
                  Payload = SessionDropped (abortedSession.SessionId);
                })
            |> List.ofSeq

        let state = { state with CoordinationSessions = sessions }

        (msgs, state)
    | Ok (CoordinationSessions.LatencyMeasured sessions) ->
        let state = { state with CoordinationSessions = sessions }

        ([], state)
    | Ok (CoordinationSessions.LastLatencyMeasured (instructions, sessions)) ->
        let msgs =
            instructions
            |> List.map (fun (participantId, instruction) ->
                { ParticipantId = participantId;
                  Payload = Instructions instruction;
                })

        let state = { state with CoordinationSessions = sessions }

        (msgs, state)

let dropExpiredSessions (instant : Instant) : StateUpdate = fun state ->
    let droppedSessionIds, deadlines =
        state.SessionDeadlines
        |> SessionDeadlines.dropExpired instant

    let droppedSessionCount = Array.length droppedSessionIds
    if droppedSessionCount > 0 then
        printfn "dropped %i expired sessions" droppedSessionCount

    let droppedSessions, activeSessions =
        state.CoordinationSessions
        |> CoordinationSessions.removeSessions droppedSessionIds

    let msgs =
        droppedSessions
        |> List.collect (fun session ->
            CoordinationSession.participantIds session
            |> Array.toSeq
            |> Seq.map (fun participantId ->
                { ParticipantId = participantId;
                  Payload = SessionDropped (CoordinationSession.id session)
                })
            |> List.ofSeq
            )

    let state =
        { state with
            CoordinationSessions = activeSessions;
            SessionDeadlines = deadlines;
        }

    (msgs, state)

let update (msg : IncomingMessage) : StateUpdate = fun state ->
    let ({ ParticipantId = participantId; Payload = payload } : IncomingMessage) = msg

    let updater =
        match payload with
        | GetRoomList channelName ->
            getRoomList participantId channelName
        | RoomJoinRequest location ->
            addParticipant participantId location
        | RoomLeaveRequest () ->
            removeParticipant participantId
        | SessionPong sessionId ->
            registerSessionParticipant participantId sessionId

    state |> updater
