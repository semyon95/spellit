module Coordinator.Dto

open FSharpPlus
open FSharpPlus.Data
open System

open Coordinator.Domain
open Coordinator.State

type ValidationErrorJson = private ValidationErrorJson of string
let (|ValidationErrorJson|) (ValidationErrorJson n) = n

module ValidationErrorJson =
    let create (value: 'a) : ValidationErrorJson =
        ValidationErrorJson (Json.serialize value)

type Error =
    | ProtocolError
    | ValidationError of ValidationErrorJson

type LocationDto = {
    Channel : string
    Word : string
    }

module LocationDto =
    let fromDomain : Location -> LocationDto =
        function
        | { Channel = (ChannelName channel); Word = (RoomWord word) } ->
            { Channel = channel; Word = word }

    let intoProto : LocationDto -> Proto.Location =
        function
        | { Channel = channel; Word = word } ->
            { channel = Some channel; word = Some word }

    let fromProto : Proto.Location -> Option<LocationDto> =
        function
        | { channel = channel; word = word } ->
            (fun channel word -> { Channel = channel; Word = word })
            <!> channel <*> word

    let intoDomain : LocationDto -> ValidationAcc<Location.ValidationError, Location> =
        function
        | { Channel = channel; Word = word } -> Location.create channel word

type RoomInfoDto = {
    Location : LocationDto
    ParticipantCount : int
    MaxParticipants : int
    }

module RoomInfoDto =
    let fromDomain : RoomInfo -> RoomInfoDto =
        function
        | { Location = location; ParticipantCount = participantCount; MaxParticipants = maxParticipants } ->
            let location = LocationDto.fromDomain location
            { Location = location; ParticipantCount = participantCount; MaxParticipants = maxParticipants }

    let intoProto : RoomInfoDto -> Proto.RoomInfo =
        function
        | { Location = location; ParticipantCount = participantCount; MaxParticipants = maxParticipants } ->
            let location = LocationDto.intoProto location
            { location = Some location; participantCount = Some participantCount; maxParticipants = Some maxParticipants }

type RoomListDto =
    { Channel : string
      Rooms : ResizeArray<RoomInfoDto>
    }

module RoomListDto =
    let fromDomain : RoomList -> RoomListDto =
        function
        | { Channel = channel; Rooms = rooms } ->
            let channel = match channel with ChannelName channel -> channel
            let rooms = rooms |> Seq.map RoomInfoDto.fromDomain |> ofSeq
            { Channel = channel; Rooms = rooms }

    let intoProto : RoomListDto -> Proto.RoomList =
        function
        | { Channel = channel; Rooms = rooms } ->
            { channel = Some channel; rooms = rooms |> map RoomInfoDto.intoProto }

type InstructionsDto =
    { SessionId : Guid
      PayloadLetter : string
      ApproxServerToClientLatencyMs : double
      BaseDelayMs : double
      OffsetDelayMs : double
      CalculatedFinalWaitTimeMs : double
    }

module InstructionsDto =
    let fromDomain : ParticipantInstructions -> InstructionsDto =
        function
        | { SessionId = sessionId;
            PayloadLetter = payloadLetter;
            ApproxServerToClientLatencyMs = approxLatency;
            BaseDelayMs = baseDelay;
            OffsetDelayMs = offsetDelay;
            CalculatedFinalWaitTimeMs = waitTime;
          } ->
            let sessionId = match sessionId with CoordinationSessionId sessionId -> sessionId
            { SessionId = sessionId;
              PayloadLetter = payloadLetter;
              ApproxServerToClientLatencyMs = approxLatency;
              BaseDelayMs = baseDelay;
              OffsetDelayMs = offsetDelay;
              CalculatedFinalWaitTimeMs = waitTime;
            }

    let intoProto : InstructionsDto -> Proto.Instructions =
        function
        | { SessionId = sessionId;
            PayloadLetter = payloadLetter;
            ApproxServerToClientLatencyMs = approxLatency;
            BaseDelayMs = baseDelay;
            OffsetDelayMs = offsetDelay;
            CalculatedFinalWaitTimeMs = waitTime;
          } ->
            { sessionId = Some (string sessionId);
              payloadLetter = Some payloadLetter;
              approxServerToClientLatencyMs = Some approxLatency;
              baseDelayMs = Some baseDelay;
              offsetDelayMs = Some offsetDelay;
              calculatedFinalWaitTimeMs = Some waitTime;
            }

type IncomingMessagePayloadDto =
    | GetRoomList of string
    | RoomJoinRequest of LocationDto
    | RoomLeaveRequest of unit
    | SessionPong of string

module IncomingMessagePayloadDto =
    type ValidationError =
        | ChannelName of ChannelName.ValidationError
        | Location of NonEmptyList<Location.ValidationError>
        | SessionPong

    let fromProto : Proto.ClientMessagePayload -> Option<IncomingMessagePayloadDto> =
        function
        | { kind = kind } ->
            kind |> Option.bind (
                function
                | Proto.ClientMessagePayload.Kind.GetRoomList channelName ->
                    Some (IncomingMessagePayloadDto.GetRoomList channelName)
                | Proto.ClientMessagePayload.Kind.RoomJoinRequest location ->
                    LocationDto.fromProto location
                    |> Option.map IncomingMessagePayloadDto.RoomJoinRequest
                | Proto.ClientMessagePayload.Kind.RoomLeaveRequest _ ->
                    Some (IncomingMessagePayloadDto.RoomLeaveRequest ())
                | Proto.ClientMessagePayload.Kind.SessionPong sessionId ->
                    Some (IncomingMessagePayloadDto.SessionPong sessionId)
                )

    let intoDomain : IncomingMessagePayloadDto -> ValidationAcc<ValidationError, IncomingMessagePayload> =
        function
        | IncomingMessagePayloadDto.GetRoomList channelName ->
            channelName
            |> ChannelName.create
            |> ValidationAcc.fromResult ValidationError.ChannelName
            |> ValidationAcc.map IncomingMessagePayload.GetRoomList

        | IncomingMessagePayloadDto.RoomJoinRequest locationDto ->
            locationDto
            |> LocationDto.intoDomain
            |> ValidationAcc.bimap ValidationError.Location IncomingMessagePayload.RoomJoinRequest

        | IncomingMessagePayloadDto.RoomLeaveRequest () ->
            IncomingMessagePayload.RoomLeaveRequest ()
            |> ValidationAcc.success

        | IncomingMessagePayloadDto.SessionPong sessionId ->
            match sessionId |> asOptionFn Guid.TryParse with
            | None ->
                ValidationError.SessionPong |> ValidationAcc.failure
            | Some sessionId ->
                IncomingMessagePayload.SessionPong (CoordinationSessionId sessionId)
                |> ValidationAcc.success

    let domainFromProto (protoDto : Proto.ClientMessagePayload) : Result<IncomingMessagePayload, Error> =
        match fromProto protoDto with
        | None -> Result.Error ProtocolError
        | Some dto ->
            match intoDomain dto with
            | Failure errors -> Result.Error (ValidationErrorJson.create errors |> ValidationError)
            | Success value -> Result.Ok value

type OutcomingMessagePayloadDto =
    | RoomList of RoomListDto
    | LeftRoom of unit
    | JoinedRoom of RoomInfoDto
    | RoomIsFull of LocationDto
    | RoomUpdated of RoomInfoDto
    | SessionPing of Guid
    | SessionDropped of Guid
    | Instructions of InstructionsDto

module OutcomingMessagePayloadDto =
    let fromDomain : OutcomingMessagePayload -> OutcomingMessagePayloadDto =
        function
        | OutcomingMessagePayload.RoomList roomList ->
            RoomList (RoomListDto.fromDomain roomList)
        | OutcomingMessagePayload.LeftRoom () ->
            LeftRoom ()
        | OutcomingMessagePayload.JoinedRoom roomUpdated ->
            JoinedRoom (RoomInfoDto.fromDomain roomUpdated)
        | OutcomingMessagePayload.RoomIsFull location ->
            RoomIsFull (LocationDto.fromDomain location)
        | OutcomingMessagePayload.RoomUpdated roomUpdated ->
            RoomUpdated (RoomInfoDto.fromDomain roomUpdated)
        | OutcomingMessagePayload.SessionPing sessionId ->
            SessionPing (match sessionId with CoordinationSessionId sessionId -> sessionId)
        | OutcomingMessagePayload.SessionDropped sessionId ->
            SessionDropped (match sessionId with CoordinationSessionId sessionId -> sessionId)
        | OutcomingMessagePayload.Instructions instructions ->
            Instructions (InstructionsDto.fromDomain instructions)

    let intoProto (dto : OutcomingMessagePayloadDto) : Proto.ServerMessagePayload =
        let kind =
            match dto with
            | RoomList roomList ->
                RoomListDto.intoProto roomList
                |> Proto.ServerMessagePayload.RoomList
            | LeftRoom () ->
                Proto.ServerMessagePayload.LeftRoom false
            | JoinedRoom roomUpdated ->
                RoomInfoDto.intoProto roomUpdated
                |> Proto.ServerMessagePayload.JoinedRoom
            | RoomIsFull location ->
                LocationDto.intoProto location
                |> Proto.ServerMessagePayload.RoomIsFull
            | RoomUpdated roomUpdated ->
                RoomInfoDto.intoProto roomUpdated
                |> Proto.ServerMessagePayload.RoomUpdated
            | SessionPing sessionId ->
                string sessionId
                |> Proto.ServerMessagePayload.SessionPing
            | SessionDropped sessionId ->
                string sessionId
                |> Proto.ServerMessagePayload.SessionDropped
            | Instructions instructions ->
                InstructionsDto.intoProto instructions
                |> Proto.ServerMessagePayload.Instructions

        { kind = Some kind }

    let protoFromDomain : OutcomingMessagePayload -> Proto.ServerMessagePayload =
        fromDomain >> intoProto

let protoFromDomain : OutcomingMessagePayload -> Proto.ServerMessage =
    OutcomingMessagePayloadDto.protoFromDomain
    >> (fun payload -> { kind = Some (Proto.ServerMessage.Ok payload) })

type DomainFromProtoResult =
    | DomainEvent of IncomingMessagePayload
    | ErrorResponseMessage of Proto.ServerMessage

let domainFromProto (protoDto : Proto.ClientMessagePayload) : DomainFromProtoResult =
    match protoDto |> IncomingMessagePayloadDto.domainFromProto with
    | Result.Ok payload -> DomainEvent payload
    | Result.Error error ->
        let kind =
            match error with
            | ProtocolError -> Proto.ServerMessage.ProtocolError false
            | ValidationError (ValidationErrorJson json) -> Proto.ServerMessage.ValidationError json
        
        ErrorResponseMessage { kind = Some kind }
