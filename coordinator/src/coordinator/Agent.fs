module Coordinator.Agent

open Coordinator
open FSharp.Control.Reactive
open NodaTime
open System

type private Agent<'Msg> = MailboxProcessor<'Msg>

type CoordinatorTx = State.IncomingMessagePayload -> unit
type ParticipantCallback = State.OutcomingMessagePayload -> unit

type private CoordinatorAgentMessage =
    private
    | ParticipantConnected of Domain.ParticipantId * ParticipantCallback
    | ParticipantDropped of Domain.ParticipantId
    | Incoming of State.IncomingMessage
    | Outcoming of State.OutcomingMessage

type CoordinatorAgent = private Agent of Agent<CoordinatorAgentMessage>
type private CoordinatorState = Coordinator.State.State

type private AgentState = private {
    ParticipantCallbacks : Map<Domain.ParticipantId, ParticipantCallback>;
    CoordinatorState : CoordinatorState;
    }

let private sendOutcomingMessage (outcomingMsg : State.OutcomingMessage) (state : AgentState) : AgentState =
    let tx = state.ParticipantCallbacks |> Map.tryFind outcomingMsg.ParticipantId
        
    match tx with
    | None -> ()
    | Some tx -> tx outcomingMsg.Payload

    state

let private handleMessage (msg : CoordinatorAgentMessage) (state : AgentState) : AgentState =
    let { ParticipantCallbacks = participantCallbacks;
          CoordinatorState = coordinatorState;
        } = state
    
    match msg with
    | ParticipantConnected (participantId, tx) ->
        let participantCallbacks = participantCallbacks |> Map.add participantId tx
        
        { ParticipantCallbacks = participantCallbacks;
          CoordinatorState = coordinatorState;
        }
    | ParticipantDropped participantId ->
        let participantCallbacks = participantCallbacks |> Map.remove participantId

        let dropMsg : State.IncomingMessage =
            { ParticipantId = participantId; Payload = State.RoomLeaveRequest () }
        let _, coordinatorState = coordinatorState |> State.update dropMsg

        { ParticipantCallbacks = participantCallbacks;
          CoordinatorState = coordinatorState;
        }
    | Incoming incomingMsg ->
        let outcomingMsgs, coordinatorState = coordinatorState |> State.update incomingMsg

        let agentState =
            { ParticipantCallbacks = participantCallbacks;
              CoordinatorState = coordinatorState;
            }

        (agentState, outcomingMsgs)
        ||> List.fold (fun agentState outcomingMsg -> sendOutcomingMessage outcomingMsg agentState)

    | Outcoming outcomingMsg ->
        sendOutcomingMessage outcomingMsg state

type LoopEvent =
    private
    | MessageReceived of CoordinatorAgentMessage
    | Cleanup

let private runAgent (agentState : AgentState) (agent : Agent<CoordinatorAgentMessage>) : Async<unit> =
    let cleanupTimer =
        Observable.interval Settings.cleanupInterval
        |> Observable.map (fun _ -> Cleanup)

    let incomingMessageStream =
        Observable.repeat (Observable.ofAsync (agent.Receive()))
        |> Observable.map MessageReceived

    let eventStream = Observable.merge cleanupTimer incomingMessageStream
    
    eventStream
    |> Observable.fold (fun agentState ->
        function
        | MessageReceived msg ->
            agentState |> handleMessage msg
        | Cleanup ->
            let instant = SystemClock.Instance.GetCurrentInstant()
            let outcomingMsgs, coordinatorState =
                agentState.CoordinatorState
                |> Coordinator.State.dropExpiredSessions instant

            let agentState = { agentState with CoordinatorState = coordinatorState }
            
            (agentState, outcomingMsgs)
            ||> List.fold (fun agentState outcomingMsg -> sendOutcomingMessage outcomingMsg agentState)
        )
        agentState
    |> Async.AwaitObservable
    |> Async.Ignore

let withCoordinator (f : CoordinatorAgent -> 'a) : 'a =
    let agentState = { ParticipantCallbacks = Map.empty; CoordinatorState = Coordinator.State.empty }
    use agent = Agent.Start (runAgent agentState)
    f (Agent agent)

let withChannel
    (agent : CoordinatorAgent)
    (withCoordinatorTx : CoordinatorTx -> Async<'a>)
    (onOutcomingMessage : State.OutcomingMessagePayload -> Async<'b>)
    (participantId : Domain.ParticipantId) : Async<'a> =

    let coordinatorAgent =
        match agent with
        | Agent a -> a

    let routerAgent = Agent.Start (fun (agent : Agent<State.OutcomingMessage>) ->
        async {
            while true do
                let! msg = agent.Receive()
                let! _ = onOutcomingMessage msg.Payload
                return ()
        })

    let participantCallback : ParticipantCallback = fun payload ->
        routerAgent.Post { ParticipantId = participantId; Payload = payload }

    ParticipantConnected (participantId, participantCallback) |> coordinatorAgent.Post

    let coordinatorTx : CoordinatorTx = fun payload ->
        let msg = Incoming { ParticipantId = participantId; Payload = payload }
        coordinatorAgent.Post msg

    async {
        let! result = withCoordinatorTx coordinatorTx
        ParticipantDropped participantId |> coordinatorAgent.Post
        (routerAgent :> IDisposable).Dispose()
        return result
    }
