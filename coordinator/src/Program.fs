﻿open Suave

open Coordinator.Agent
open Server

[<EntryPoint>]
let main _ =
    withCoordinator (fun coordinator ->
        let context = { Coordinator = coordinator }
        
        app context
        |> startWebServer
            { defaultConfig with
                maxOps = Settings.maxSocketOps
                maxContentLength = Settings.maxContentLength
            }
        )

    0
