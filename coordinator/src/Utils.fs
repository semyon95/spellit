[<AutoOpen>]
module Utils

open FSharpPlus.Data
open Microsoft.FSharpLu.Json
open Newtonsoft.Json
open Newtonsoft.Json.Serialization
open System
open System.Threading

type ValidationAcc<'e, 'a> = Validation<NonEmptyList<'e>, 'a>

module ValidationAcc =
    let success (value : 'a) : ValidationAcc<'e, 'a> =
        Success value

    let failure (err : 'e) : ValidationAcc<'e, 'a> =
        Failure (NonEmptyList.singleton err)

    let bimap (fErr : NonEmptyList<'e1> -> 'e2) (fValue : 'a -> 'b) : ValidationAcc<'e1, 'a> -> ValidationAcc<'e2, 'b> =
        Validation.bimap (fErr >> NonEmptyList.singleton) fValue

    let map (f : 'a -> 'b) : ValidationAcc<'e, 'a> -> ValidationAcc<'e, 'b> =
        Validation.map f

    let mapErrors (f : NonEmptyList<'e1> -> 'e2) : ValidationAcc<'e1, 'a> -> ValidationAcc<'e2, 'a> =
        Validation.bimap (f >> NonEmptyList.singleton) id

    let fromResult (f : 'e1 -> 'e2) : Result<'a, 'e1> -> ValidationAcc<'e2, 'a> =
        function
        | Ok value -> success value
        | Error err -> failure (f err)

let asOptionFn (f : 'a -> bool * 'b) : 'a -> Option<'b> =
    fun a ->
        let (ok, b) = f a
        if ok then Some b else None

module Json =
    type Settings =
        static member formatting = Formatting.None
        static member settings =
            JsonSerializerSettings(
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = DefaultContractResolver(
                    NamingStrategy = CamelCaseNamingStrategy(
                        OverrideSpecifiedNames = true
                    )
                ),
                Converters = [| CompactUnionJsonConverter(true) |]
            )

    type private S = With<Settings>

    let inline serialize< ^T> x = S.serialize x

    let inline deserialize< ^T> json : ^T = S.deserialize< ^T> json

let private synchronize f = 
    let ctx = System.Threading.SynchronizationContext.Current 
    f (fun g ->
        let nctx = System.Threading.SynchronizationContext.Current 
        if ctx <> null && ctx <> nctx then ctx.Post((fun _ -> g()), null)
        else g() )

type Microsoft.FSharp.Control.Async with
    /// Creates an asynchronous workflow that will be resumed when the 
    /// specified observables produces a value. The workflow will return 
    /// the value produced by the observable.
    static member AwaitObservable(observable : IObservable<'T1>) =
        let removeObj : IDisposable option ref = ref None
        let removeLock = new obj()
        let setRemover r = 
            lock removeLock (fun () -> removeObj := Some r)
        let remove() =
            lock removeLock (fun () ->
                match !removeObj with
                | Some d -> removeObj := None
                            d.Dispose()
                | None   -> ())
        synchronize (fun f ->
        let workflow =
            Async.FromContinuations((fun (cont,econt,ccont) ->
                let rec finish cont value =
                    remove()
                    f (fun () -> cont value)
                setRemover <|
                    observable.Subscribe
                        ({ new IObserver<_> with
                            member x.OnNext(v) = finish cont v
                            member x.OnError(e) = finish econt e
                            member x.OnCompleted() =
                                let msg = "Cancelling the workflow, because the Observable awaited using AwaitObservable has completed."
                                finish ccont (new System.OperationCanceledException(msg)) })
                () ))
        async {
            let! cToken = Async.CancellationToken
            let token : CancellationToken = cToken
            use registration = token.Register((fun _ -> remove()), null)
            return! workflow
        })