module App.Common.ListGroup

open Fable.Core.JsInterop
open Fable.React
open Fable.React.Props

importDefault "./ListGroup.scss"

let ListGroup (children : seq<ReactElement>) : ReactElement =
    ul [ ClassName "ListGroup" ] children

let ListGroupItem (children : seq<ReactElement>) : ReactElement =
    li [ ClassName "ListGroup__Item" ] children
