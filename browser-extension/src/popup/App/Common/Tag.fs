module App.Common.Tag

open Fable.Core.JsInterop
open Fable.React
open Fable.React.Props

importDefault "./Tag.scss"

let Tag (children : seq<ReactElement>) : ReactElement =
    span [ ClassName "Tag" ] [
        span [ ClassName "Tag__Content" ] children
    ]
