module App.Common.Button

open Fable.Core.JsInterop
open Fable.React
open Fable.React.Props

importDefault "./Button.scss"

type ButtonModifier =
    | Borderless

let private applyModifier (className : string) (modifier : ButtonModifier) : string =
    match modifier with
    | Borderless -> className + " Button--Borderless"

let Button (modifiers : Set<ButtonModifier>) (children : seq<ReactElement>) : ReactElement =
    let className = modifiers |> Set.fold applyModifier "Button"

    button [ ClassName className ] children
