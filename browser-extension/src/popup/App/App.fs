module App

open Fable.Core.JsInterop
open Fable.React
open Fable.React.Props

open App.ChannelPage

importDefault "./App.scss";

type AppProps = {
    ChannelName : Option<string>
}

let App (props : AppProps) : ReactElement =
    let ({ ChannelName = channelName }: AppProps) = props

    div [ ClassName "App" ] [
        ChannelPage { ChannelName = channelName }
    ]
