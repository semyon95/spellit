module App.ChannelPage.WordEntry

open Fable.Core.JsInterop
open Fable.React
open Fable.React.Props

open App.Common.Button

importDefault "./WordEntry.scss";

type WordEntryProps = {
    Word : string
    ParticipantCount : int
}

let WordEntry (props : WordEntryProps) : ReactElement =
    let { Word = word; ParticipantCount = participantCount } = props

    let wordLength = String.length word

    div [ ClassName "WordEntry" ] [
        div [ ClassName "WordEntryInfo" ] [
            span [ ClassName "WordEntryInfo__Counts" ] [
                str (sprintf "%i/%i" participantCount wordLength)
            ]
            span [ ClassName "WordEntryInfo__Word" ] [ str word ]
        ]
        Button Set.empty [ str "Join" ]
    ]

