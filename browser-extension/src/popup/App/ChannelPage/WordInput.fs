module App.ChannelPage.WordInput

open Fable.Core.JsInterop
open Fable.React
open Fable.React.Props

open App.Common.Button

importDefault "./WordInput.scss";

type WordInputProps = unit

let WordInput (props : WordInputProps) : ReactElement =
    div [ ClassName "WordInput" ] [
        div [ ClassName "WordInputField" ] [
            input [ ClassName "WordInputField__Input" ]
            Button (set [ ButtonModifier.Borderless ])[ str "Join" ]
        ]
    ]
