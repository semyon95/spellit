module App.ChannelPage

open Fable.Core.JsInterop
open Fable.React
open Fable.React.Props

open App.ChannelPage.WordEntry
open App.ChannelPage.WordInput
open App.Common.Tag
open App.Common.ListGroup

importDefault "./ChannelPage.scss";

type ChannelPageProps = {
    ChannelName : Option<string>
}

let ChannelPage (props : ChannelPageProps) : ReactElement =
    let ({ ChannelName = channelName }: ChannelPageProps) = props

    let heading =
        let content =
            match channelName with
            | None ->
                [ str "You are not in a Twitch channel" ]
            | Some channelName ->
                [
                    str "You are in "
                    Tag [ str ("#" + channelName) ]
                ]

        div [ ClassName "ChannelHeading"] [
            div [ ClassName "ChannelHeading__Content" ] content
        ]

    let restSections =
        match channelName with
        | Some _ ->
            let wordInput = WordInput ()

            let wordList =
                ListGroup [
                    ListGroupItem [
                        WordEntry { Word = "HYPERS"; ParticipantCount = 5 }
                    ]
                    ListGroupItem [
                        WordEntry { Word = "POGGERS"; ParticipantCount = 4 }
                    ]
                    ListGroupItem [
                        WordEntry { Word = "TRIHARD"; ParticipantCount = 3 }
                    ]
                ]

            [ wordInput; wordList ]

        | None -> []

    div [ ClassName "ChannelPage" ] (heading :: restSections)
