module Main

open Browser.Dom
open Fable.Core
open Fable.Core.JsInterop
open Fable.React
open System

open App
open WebExtension

importDefault "./index.scss"

type AppContext = {
    ChannelName : Option<string>
}

let extractChannelName (url : string) : Option<string> =
    if String.IsNullOrWhiteSpace url then
        None
    else
        let revSegments =
            url.Split([|'/'|], StringSplitOptions.RemoveEmptyEntries)
            |> Array.rev
            |> Seq.truncate 2
            |> Array.ofSeq

        match revSegments with
        | [| channelName; hostname |] ->
            if hostname = "twitch.tv" || hostname = "www.twitch.tv"
            then Some channelName
            else None
        | _ -> None

let getAppContext () : JS.Promise<AppContext> =
    promise {
        let! tabs = browser.tabs.query { active = Some true; currentWindow = Some true }
        
        let channelName =
            tabs
            |> Array.tryHead
            |> Option.bind (fun t -> t.url)
            |> Option.bind extractChannelName

        return { ChannelName = channelName }
    }

[<EntryPoint>]
let main _ =
    promise {
        let! appCtx = getAppContext ()

        let mountPoint = document.getElementById "app"

        let appProps = { ChannelName = appCtx.ChannelName }
        let element = ReactBindings.React.createElement (App, appProps, [])

        ReactDom.render (element, mountPoint)
    } |> ignore

    0
