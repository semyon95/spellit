module WebExtension

open Fable.Core

type QueryInfo = {
    active : Option<bool>
    currentWindow : Option<bool>
}

type ITab =
    abstract id : Option<int>
    abstract url : Option<string>

type ITabs =
    abstract query : QueryInfo -> JS.Promise<ITab[]>

type IBrowser =
    abstract tabs : ITabs

[<Import("default", from="webextension-polyfill")>]
let browser : IBrowser = jsNative
