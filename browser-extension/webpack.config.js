const path = require("path");

//const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const CleanPlugin = require("clean-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const HtmlPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const config = {
    babel: {
        presets: ["@babel/preset-env"]
    }
};

const resolve = filePath =>
    path.isAbsolute(filePath) ? filePath : path.join(__dirname, filePath);

const getStyleLoader = mode =>
    mode === "production" ? MiniCssExtractPlugin.loader : "style-loader";

module.exports = (env, options) => ({
    entry: {
        "popup/index": resolve("src/popup/Popup.fsproj"),
        "content-script/index": resolve(
            "src/content-script/ContentScript.fsproj"
        )
    },
    output: {
        filename: "[name].js",
        path: resolve("dist")
    },
    module: {
        rules: [
            {
                test: /\.(fs|fsproj)$/,
                use: {
                    loader: "fable-loader",
                    options: {
                        babel: config.babel
                    }
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: config.babel
                }
            },
            {
                test: /\.scss$/,
                use: [
                    getStyleLoader(options.mode),
                    "css-loader",
                    {
                        loader: "sass-loader",
                        options: {
                            implementation: require("sass"),
                            fiber: require("fibers")
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        //new BundleAnalyzerPlugin(),
        new CleanPlugin({
            cleanStaleWebpackAssets: !options.watch
        }),
        new CopyPlugin([{ from: resolve("src/manifest.json") }]),
        new HtmlPlugin({
            template: resolve("src/popup/index.ejs"),
            filename: "popup/index.html",
            chunks: ["popup/index"]
        }),
        new MiniCssExtractPlugin()
    ],
    optimization: {
        usedExports: true,
        splitChunks: {
            cacheGroups: {
                vendors: {
                    test: /[\\/](node_modules|\.fable)[\\/]/,
                    chunks: "all",
                    name: "vendors"
                }
            }
        }
    },
    devtool: options.mode === "production" ? "source-map" : "inline-source-map"
});
