yarn install

pushd src/popup
paket install
dotnet restore
popd

pushd src/content-script
paket install
dotnet restore
popd
